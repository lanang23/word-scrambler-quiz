<?php if (! defined('BASEURL')) die('Forbidden');

/* Created by	: Yudha Rachmat Hidayat
 * Email		: email@yudha.web.id
 * Website		: https://yudha.id
 * 
 * */

// Sebelum pecah jadi segment, maka harus diketahui dulu path infonya.
$pattern = Array('~');
$replace = Array('');

// Karena tidak semua server ada $_SERVER['PATH_INFO'], maka buat path_info manual
$without_file = str_replace($pattern, $replace, rtrim($_SERVER['SCRIPT_NAME'], 'index.php'));
$uri = str_replace($pattern, $replace, $_SERVER['REQUEST_URI']);
$path_info = preg_replace("~^{$without_file}~", '/', $uri);

$segment = explode('/', $path_info);
$segment_count = count($segment);


// Kalau tidak ada path info atau segment[1] == '', maka pake route default
if (false === isset($segment_count) || $segment[1] == '') {
	$segment = Array('', yudha_config::$default_route);
	$segment_count = count($segment);
}

try {
	// panggil controller
	$controller_name = ucfirst(strtolower($segment[1]));
	$controller_file = "{$controller_name}.php";
	$controller_file_path = APPDIR.'/controllers/'.$controller_file;
	
	if (false === file_exists($controller_file_path))
		throw new Exception();
	
	require($controller_file_path);
	
	$ctrl = new $controller_name();
	
	// panggil route method
	if (isset($segment[2]) && trim($segment[2]) != '') {
		$method = 'route_'.strtolower($segment[2]);
		
		if (false === method_exists($ctrl, $method))
			throw new Exception();
		
		$arr_param = Array();
		
		// kalau segment countnya lebih dari 3, maka itu dianggap sebagai parameter
		if ($segment_count > 3) {
			
			// ambil parameter
			for ($i = 3; $i < $segment_count; $i++) {
				$arr_param[] = $segment[$i];
			}
		}
		
		// eksekusi method beserta parameternya			
		call_user_func_array(Array($ctrl, $method), $arr_param);
	}
	// Kalau tidak ada segment[2] maka menggunakan route_index
	else {
		$method = 'route_index';
		
		if (false === method_exists($ctrl, $method))
			throw new Exception();
			
		call_user_func(Array($ctrl, $method));
	}
}
catch (Exception $e) {
	echo 'Halaman tidak ditemukan';
}
