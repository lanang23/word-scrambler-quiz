<?php if (! defined('BASEURL')) die('Forbidden');

/* Created by	: Yudha Rachmat Hidayat
 * Email		: email@yudha.web.id
 * Website		: https://yudha.id
 * 
 * */

// Langsung buat session
session_start();

class Yudha {
		
	// Load Database, etc
	public static function load($type) {
		switch ($type) {
			case 'database': 
				require_once SYSTEMDIR.'/database_handler.php';
			break;
			
			case 'csrf':
				require_once SYSTEMDIR.'/csrf_handler.php';
				csrf::init();
			break;
			
			default: echo 'Load Handler tidak ditemukan!';
		}
	}
	
	// Load Model
	public static function model($model_name) {
		$model_name = ucfirst(strtolower($model_name));
		
		require_once APPDIR.'/models/'.$model_name.'.php';
		
		return new $model_name;
	}
	
	public static function view($filename, $data = Array()) {
		// bisa saja user menulis dengan .php maka kita harus periksa dulu. kalau ternyata tidak menulis .php maka tambahkan .php di belakangnya
		
		if (! preg_match('/\.php$/', $filename))
			$filename .= '.php';
		
		// kalau data ada isinya, maka ubah menjadi variable
		if (count($data) > 0) {
			foreach ($data as $key => $val) {
				${$key} = $val;
			}
		}
		
		include APPDIR.'/views/'.$filename;
	}
}
