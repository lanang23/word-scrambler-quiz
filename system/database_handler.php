<?php if (! defined('BASEURL')) die('Forbidden');

/* Created by	: Yudha Rachmat Hidayat
 * Email		: email@yudha.web.id
 * Website		: https://yudha.id
 * 
 * */
 
class Db {
	
	static $handle;
	
	public static function sqlite_open()
	{
		require APPDIR.'/database.php';
		
		return new SQLite3($db_config['dbname']);
	} 
	
	public static function trans_begin() {
		self::query('BEGIN TRANSACTION;');
	}
	
	public static function trans_commit() {
		self::query('COMMIT TRANSACTION;');
	}
	
	public static function trans_rollback() {
		self::query('ROLLBACK TRANSACTION;');
	}
	
	public static function query($query) {
		$handle = self::sqlite_open();
		return $handle->query($query);
	}
	
	public static function query_single($query) {
		$handle = self::sqlite_open();
		return $handle->querySingle($query, true);
	}
}
