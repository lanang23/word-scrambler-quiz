<?php if (! defined('BASEURL')) die('Forbidden');

/* Created by	: Yudha Rachmat Hidayat
 * Email		: email@yudha.web.id
 * Website		: https://yudha.id
 * 
 * */
 
class Csrf {
	static $token;
	
	public static function generate() {	
		return self::$token;
	}
	
	public static function init() {
		
		if (isset($_POST['csrf_token']) && isset($_SESSION['_csrf_token'])) {
			
			// kalau ada post dan session baru dicheck apakah mereka sama
			if ($_POST['csrf_token'] != $_SESSION['_csrf_token'])
				die('Token Expired');
		}
		elseif ($_POST) {
			die('Token Not Found');
		}
		
		self::$token = sha1('yudha'.time());
		$_SESSION['_csrf_token'] = self::$token;
	}
}
