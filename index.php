<?php
/* Author  : Yudha Rachmat Hidayat
 * Email   : email@yudha.web.id
 * Website : https://yudha.id
 * 
 * */

define('APPDIR', 'app');
define('SYSTEMDIR', 'system');

/*------------ ini untuk menentukan base url saat nanti membuat url */
$url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
define('BASEURL', preg_replace("/index\.php$/", '', (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[SCRIPT_NAME]"));
//------------

/* ******** URUTAN HANDLER TIDAK BOLEH DIUBAH! ************ */ 

// Panggil Config
require(APPDIR.'/config.php');

// SITE URL
define('SITEURL', preg_replace('/\/\/$/', '/', BASEURL.yudha_config::$index_page.'/'));

// Panggil Handler Lainnya
require(SYSTEMDIR.'/yudha_handler.php');

// Panggil URI Handler
require(SYSTEMDIR.'/uri_handler.php');
