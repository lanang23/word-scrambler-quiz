/* Created by	: Yudha Rachmat Hidayat
 * Email		: email@yudha.web.id
 * Website		: https://yudha.id
 * 
 * */

$(function(){
	$.ajaxSetup({
		beforeSend: function(){
			$('#loading').fadeIn();
		},
		complete: function(){
			$('#loading').fadeOut(function(){
				$('#answer').focus();
			});
		}
	})
	
	$('#start').on('click', function(){
		$.post(siteurl+'ajax/get_word', {'csrf_token': $('input[name=csrf_token]').val()}, function(data) {
			$('input[name=csrf_token]').val(data.csrf_token);
			
			$('#welcome').hide();
			$('#quiz').show();

			$('#question').html(write(data.word))
			$('#score').html(data.point)
			
			if (data.welcomeback) {
				$('#welcomeback').show();
			}
			
			(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.9";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
		}, 'json');
	})
	
	$('#answer_button').on('click', function(){
		$.post(siteurl+'ajax/check_answer', {'csrf_token': $('input[name=csrf_token]').val(), 'answer': $('#answer').val()}, function(data) {
			$('input[name=csrf_token]').val(data.csrf_token);

			if (data.msg) {
				alert(data.msg)
				$('#add').removeClass('green').addClass('red').html(data.add)
			} else if (data.correct) {
				alert(data.correct)
				$('#add').removeClass('red').addClass('green').html(data.add)
			}
			
			$('#add').show().fadeOut(1000, function(){
				$('#score').html(data.point)
			});
			
			if (data.word) {
				$('#question').html(write(data.word))
			}
			
			$('#answer').val('');
			
			if (data.welcomeback) {
				$('#welcomeback').show();
			}
		}, 'json');
	})
	
	$('#answer').on('keyup', function(e){
		var keycode = (e.keyCode ? e.keyCode : e.which);
		if (keycode == '13') {
			if ($('#alert-dialog').is(':visible'))
				$('#alert-dialog .alert-dialog-container > button').trigger('click')
			else
				$('#answer_button').trigger('click').focus();
		}
	})
	
	function write(word) {		
		return word;
	}
})

function downloadFile() {
	$('#text-loading').html('Preparing database. Please Wait ...');
	
	$.post(siteurl+'ajax/download_file', {'csrf_token': $('input[name=csrf_token]').val()}, function(data) {
		$('input[name=csrf_token]').val(data.csrf_token);
		
		if (data.msg) {
			alert(data.msg)
		}
		
		if (continueQuiz) {
			$('#start').trigger('click');
		}
		
		$('#text-loading').html('Please Wait ...');
	}, 'json')
}

// overwrite alert dialog
function alert(text) {
	$('#alert-dialog').show();
	
	$('#alert-dialog .alert-dialog-container > p')
		.html(text)
		.next()
		.unbind()
		.on('click', function(){
			$('#alert-dialog').fadeOut();
		}).focus();
}
