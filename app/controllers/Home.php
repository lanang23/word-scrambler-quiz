<?php if (! defined('BASEURL')) die('Forbidden');
/* yang dianggap sebagai url hanya method dengan prefix route_ */

// load database
yudha::load('database');

class Home {
	
	static $dir = 'data/';

	public function __construct() {
		$writable_error = 'Please make sure \''.self::$dir.'\' folder is writable';
		
		// Bikin tempat penyimpanan file words dari internet
		if (! file_exists(self::$dir)) {
			if (! is_writable(self::$dir)) die($writable_error);
			
			mkdir(self::$dir);
		}
		
		if (! is_writable(self::$dir)) die($writable_error);
		
		// Jika database belum dibuat maka dibuat dulu
		yudha::model('users')->create();
		yudha::model('score')->create();
	}
	
	public function route_index() {
		// load csrf
		yudha::load('csrf');
		
		$url = yudha_config::$word_file;
		$filename = basename($url);
		$download_file = false;
		
		// Kalau file belum ada, maka download dulu
		if (! file_exists(self::$dir.$filename)) {
			$download_file = true;
		}
		
		// load model sesuai kebutuhan
		$score = yudha::model('score');
		$users = yudha::model('users');
		
		$data = Array(
			'title' => 'Welcome',
			'csrf' => csrf::generate(),
			'continue' => isset($_SESSION['point']) ? true : false,
			'download_file' => $download_file,
			'player_id' => session_id(),
		);
		
		// Load View
		yudha::view('Home/index_view', $data);
	}
	
	public function route_share($player_id) {
		$score = yudha::model('score');
		if ($row = $score->getBySessionId($player_id)) {
			$data = Array(
				'title' => 'Share',
				'score' => $row['score'],
				'player_id' => $player_id,
			);
		}
		else
			die('Player not found');
			
		// Load View
		yudha::view('Home/share_view', $data);
	}
}
