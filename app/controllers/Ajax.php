<?php if (! defined('BASEURL')) die('Forbidden');
/* yang dianggap sebagai url hanya method dengan prefix route_ */

// load csrf
yudha::load('csrf');

// load database
yudha::load('database');

class Ajax {
	
	static $dir = 'data/';
	static $url = '';
	
	public function __construct() {
		if (false === isset($_SESSION['point'])) {
			$_SESSION['point'] = 0;
		}
		
		self::$url = yudha_config::$word_file;
	}
	
	private function same_letters($scrambled, $textone, $texttwo) {
		if ($textone == $scrambled) return false;
		
		$arr1 = str_split($textone);
		$arr2 = str_split($texttwo);

		sort($arr1);
		sort($arr2);

		$text1Sorted = implode('', $arr1);
		$text2Sorted = implode('', $arr2);

		if ($text1Sorted == $text2Sorted) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public function route_check_answer() {
		$json = Array();
		$json['error'] = false;
		$json['csrf_token'] = csrf::generate();
		
		try {
			// Kalau tidak ada $_POST[answer] atau jawabannya salah, maka beritau kalau salah
			if (! isset($_POST['answer']) || strtolower($_POST['answer']) != $_SESSION['word']) {
				
				$still_incorrect = true;
				
				// Kelamaan, dihilangkan saja
				/*
				// Tunggu dulu, coba lihat susunan hurufnya apakah sama? bisa jadi ada kata yang mirip
				if (self::same_letters($_SESSION['scrambled'], strtolower($_POST['answer']), $_SESSION['word'])) {
					$filename = basename(self::$url);
					
					$handle = fopen(self::$dir.$filename, "r");
					if ($handle) {
						while (($text = fgets($handle)) !== false) {
							// Kita hanya mengambil kata yang terdiri dari huruf saja
							if (! preg_match('/^[a-z]*$/', $text)) continue;
						
							// Kalau ketemu kata yg sama seperti susunan ini maka dianggap benar
							if (strtolower($text) == strtolower($_POST['answer'])) {
								$still_incorrect = false;
								break;
							}
						}

						fclose($handle);
					}
				}
				*/
				
				if ($still_incorrect) {
					$_SESSION['point'] -= 1;
					$json['add'] = '-1';
					
					throw new exception('Incorrect Answer. <br /><span class="red">'.strtoupper($_SESSION['word']).'</span>');
				}
				else {
					$_SESSION['point'] += 1;
					$json['add'] = '+1';
					
					$json['point'] = $_SESSION['point'];
					$json['correct'] = '<span class="green">Correct!</span>';
					
					$json['word'] = self::scramble();
				}
			}
			else {
				$_SESSION['point'] += 1;
				$json['add'] = '+1';
				
				$json['point'] = $_SESSION['point'];
				$json['correct'] = '<span class="green">Correct!</span>';
				
				$json['word'] = self::scramble();
			}
		}
		catch (Exception $e) {
			$json['error'] = true;
			$json['msg'] = $e->getMessage();
			$json['word'] = self::scramble();
		}
		
		$json['point'] = $_SESSION['point'];
		$json['welcomeback'] = isset($_SESSION['comeback']) ? true : false;
		
		// Update score
		$score = yudha::model('score');
		$score->update($_SESSION['point']);
		
		echo json_encode($json);
	}
	
	public function route_download_file() {
		$json = Array();
		$json['error'] = false;
		$json['csrf_token'] = csrf::generate();
		
		try {
			// Jika belum ada database words maka ambil dulu dari internet
			
			$filename = basename(self::$url);
			
			// Kalau file belum ada, maka download dulu
			if (! file_exists(self::$dir.$filename)) {
				$content = file_get_contents(self::$url);
				
				$fp = fopen(self::$dir.$filename, 'w');
				fwrite($fp, $content);
				fclose($fp);
			}
		}
		catch (Exception $e) {
			$json['error'] = true;
			$json['msg'] = $e->getMessage();
		}
		
		echo json_encode($json);
		
	}
	
	public function route_get_word() {
		$json = Array();
		$json['error'] = false;
		$json['csrf_token'] = csrf::generate();
		
		try {
			$json['word'] = self::scramble();
			
			// Melanjutkan score untuk session_id yang sama
			$score = yudha::model('score');
			$score->resume();
		}
		catch (Exception $e) {
			$json['error'] = true;
			$json['msg'] = $e->getMessage();
		}
		
		$json['point'] = $_SESSION['point'];
		$json['welcomeback'] = isset($_SESSION['comeback']) ? true : false;
		
		echo json_encode($json);
	}
	
	private function scramble() {
		
		// Baca File 
		$filename = basename(self::$url);
		$content = file_get_contents(self::$dir.$filename);
		$chunk = explode("\n", $content);
		$clean = Array();
		
		// Kita hanya mengambil kata yang terdiri dari huruf saja
		foreach ($chunk as $key => $text) {
			if (! preg_match('/^[a-z]*$/', $text)) continue;
			
			$clean[] = $text;
		}
		
		$num_rows = count($clean);
		$index = rand(0, $num_rows);
		
		foreach ($clean as $key => $text) {
			if ($key != $index) continue;
			if ($key == $index) {
				$word = strtolower($text);
				break;
			}
		}
		
		// terlalu rumit, ga bisa ditebak
		//$scrambled = $word;//str_shuffle($word);
		
		// Pake ini saja yang sederhana
		if (strlen($word) < 4)
            $scrambled = str_shuffle($word);
        else
            $scrambled = $word{0} . str_shuffle(substr($word, 1, -1)) . $word{strlen($word) - 1};

		$_SESSION['word'] = $word;
		$_SESSION['scrambled'] = $scrambled;
		
		return $scrambled;
	}
}
