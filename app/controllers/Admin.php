<?php if (! defined('BASEURL')) die('Forbidden');
/* yang dianggap sebagai url hanya method dengan prefix route_ */

// load csrf
yudha::load('csrf');

// load database
yudha::load('database');

class Admin {
	
	static $dir = 'data/';

	public function __construct() {
		$writable_error = 'Please make sure \''.self::$dir.'\' folder is writable';
		
		// Bikin tempat penyimpanan file words dari internet
		if (! file_exists(self::$dir)) {
			if (! is_writable(self::$dir)) die($writable_error);
			
			mkdir(self::$dir);
		}
		
		if (! is_writable(self::$dir)) die($writable_error);
		
		// Jika database belum dibuat maka dibuat dulu
		yudha::model('users')->create();
		yudha::model('score')->create();
	}
	
	public function route_dashboard() {
		if (false === isset($_SESSION['id'])) {
			header('location: '.SITEURL.'admin');
			exit;
		}
		
		$score = yudha::model('score');
		
		$data = Array(
			'title' => 'Dahsboard Page',
			'data' => $score->getAll(),
		);
		
		yudha::view('Admin/dashboard_view', $data);
	}
	
	public function route_index() {
		$users = yudha::model('users');
		$users->createAdmin();
		
		if (isset($_POST['username'], $_POST['password'])) {
			if ($result = $users->get($_POST['username'], sha1($_POST['password']))) {
				// Buat Session
				$_SESSION['id'] = $result['id'];
				$_SESSION['username'] = $result['username'];
				
				header('location: '.SITEURL.'admin/dashboard');
				exit;
			}
		}
		
		$data = Array(
			'title' => 'Admin Page',
			'csrf' => csrf::generate(),
		);
		
		yudha::view('Admin/index_view', $data);
	}
	
	public function route_logout() {
		session_destroy();
		
		header('location: '.SITEURL.'admin');
		exit;
	}
}
