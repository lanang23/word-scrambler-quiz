<?php if (! defined('BASEURL')) die('Forbidden');
/* MODEL */

class Score {
	
	public static function create() {
		db::query('CREATE TABLE IF NOT EXISTS score (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, session_id TEXT NOT NULL, score INTEGER NOT NULL, email TEXT NOT NULL)');
	}
	
	public static function getAll() {
		$session_id = session_id();
		return db::query("SELECT * FROM score ORDER BY score DESC");
	}
	
	public static function getBySessionId($player_id) {
		$player_id = preg_replace("/'/", "''", $player_id);
		return db::query_single("SELECT score FROM score WHERE session_id = '{$player_id}'");
	}
	
	public static function resume() {
		$session_id = session_id();
		
		if ($row = db::query_single("SELECT score FROM score WHERE session_id = '{$session_id}'")) {
			$_SESSION['point'] = $row['score'];
			$_SESSION['comeback'] = true;
		}
	}
	
	public static function update($score) {
		$session_id = session_id();
		$result = db::query_single("SELECT session_id FROM score WHERE `session_id` = '{$session_id}' LIMIT 1");
		
		// Kalo tidak ada datanya maka insert
		if (count($result) == 0) {
			return db::query("INSERT INTO score (`session_id`, `score`, `email`) VALUES ('{$session_id}', '{$score}', '')");
		}
		else {
			return db::query("UPDATE score SET `score`='{$score}' WHERE `session_id`='{$session_id}'");
		}
	}
}
