<?php if (! defined('BASEURL')) die('Forbidden');
/* MODEL */

class Users {
	
	public static function create() {
		db::query('CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, username TEXT NOT NULL UNIQUE, password TEXT NOT NULL)');
	}
	
	public static function createAdmin() {
		$result = db::query_single('SELECT username FROM users LIMIT 1');
		
		// Kalo tidak ada datanya maka insert
		if (count($result) == 0) {
			$username = 'admin';
			$password = sha1('admin');
			
			db::query("INSERT INTO users (`username`, `password`) VALUES ('{$username}', '{$password}')");
		}
	}
	
	public function get($username, $password) {
		$username = preg_replace("/'/", "''", $username);
		$password = preg_replace("/'/", "''", $password);
		$query = "SELECT * FROM users WHERE `username` = '{$username}' AND `password` = '{$password}'";
		
		return db::query_single($query);
	}
}
