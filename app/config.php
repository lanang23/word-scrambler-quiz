<?php if (! defined('BASEURL')) die('Forbidden');

/* Created by	: Yudha Rachmat Hidayat
 * Email		: email@yudha.web.id
 * Website		: https://yudha.id
 * 
 * */
 
class Yudha_config {
	// default: index.php, tapi jika menggunakan htaccess rewrite maka ini dikosongi
	public static $index_page = '';
	
	// controller default, jadi saat pertama kali membuka applikasi maka controller yang dibuka adalah ini
	public static $default_route = 'home';
	
	public static $word_file = 'https://github.com/dwyl/english-words/raw/master/words.txt';
}
