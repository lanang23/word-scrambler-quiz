<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?php echo $title;?></title>
	<meta name="author" content="Yudha Rachmat Hidayat" />
	<meta name="designer" content="Yudha Rachmat Hidayat | email [at] yudha.web.id" />
	<link rel="stylesheet" href="<?php echo BASEURL;?>assets/css/yudha.css" />
</head>
<body>
	<div class="as-table">
		<div class="as-td" align="center">
			<a href="<?php echo BASEURL;?>admin/logout">Logout</a>
			
			<table class="table-admin">
				<thead>
					<tr>
						<th>Player ID</th>
						<th>Score</th>
					</tr>
				</thead>
				<tbody>
					<?php while ($row = $data->fetchArray()):?>
					<tr>
						<td><?php echo $row['session_id'];?></td>
						<td><?php echo $row['score'];?></td>
					</tr>
					<?php endwhile;?>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>
