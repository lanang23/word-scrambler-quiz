<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?php echo $title;?></title>
	<meta name="author" content="Yudha Rachmat Hidayat" />
	<meta name="designer" content="Yudha Rachmat Hidayat | email [at] yudha.web.id" />
	<link rel="stylesheet" href="<?php echo BASEURL;?>assets/css/yudha.css" />
</head>
<body>
	<div class="as-table">
		<div class="as-td" align="center">
			<form method="POST">
				<input type="hidden" name="csrf_token" value="<?php echo $csrf;?>" />
				<div>
					<input type="text" class="input-text" name="username" placeholder="Username" autofocus="autofocus" />
				</div>
				<div>
					<input type="password" class="input-text" name="password" placeholder="Password" />
				</div>
				<button id="start" class="start-button">Login</button>
			</form>
			<br />
			<a href="<?php echo SITEURL;?>">Home</a>
		</div>
	</div>
</body>
</html>
