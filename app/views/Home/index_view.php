<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?php echo $title;?></title>
	<meta name="author" content="Yudha Rachmat Hidayat" />
	<meta name="designer" content="Yudha Rachmat Hidayat | email [at] yudha.web.id" />
	<link rel="stylesheet" href="<?php echo BASEURL;?>assets/css/yudha.css" />
</head>
<body>
	<div id="fb-root"></div>

	<div class="reload-container" style="display: none;" id="loading">
		<div class="reload-text as-table">
			<div class="as-td" id="text-loading">
				Please Wait ...
			</div>
		</div>
	</div>
	
	<div class="reload-container" style="display: none;" id="alert-dialog">
		<div class="reload-text as-table full">
			<div class="as-td">
				<div class="alert-dialog-container">
					<p></p>
					<button type="button">OK</button>
				</div>
			</div>
		</div>
	</div>
	
	<div class="as-table" id="welcome" style="<?php echo $continue ? 'display:none' : '';?>">
		<div class="as-td" align="center">
			<h1>Welcome to Word Scrambler Quiz</h1>
			<p class="justify">
				Objective:
				<ul class="justify">
					<li>You will be given a scrambled word and you must rearrange into meaningful word.</li>
					<li>For every word unscrambled correctly, you will get <span class="green">+1</span> point.</li>
					<li>And every incorrect answer, your score will be deducted by <span class="red">1</span> point.</li>
				</ul>
			</p>
			<button id="start" class="start-button">Start</button>
		</div>
	</div>
	
	<div class="as-table" id="quiz" style="display: none;">
		<div class="as-td" align="center">
			<div style="display: none;" id="welcomeback">
				Welcome Back,
			</div>
			<div class="player-id-container">
				Player ID : <span><?php echo $player_id;?></span>
			</div>
			<div class="score-container">
				Score : <span id="score">0</span><span id="add"></span>
			</div>
			<div>
				Scrambled Word : 
			</div>
			<div id="question" class="question">
				question
			</div>
			<div>
				<input type="text" id="answer" class="answer" placeholder="Your answer" />
			</div>
			<div>
				<button type="button" id="answer_button" class="answer_button">Answer</button>
			</div>
			<div style="margin-top: 20px;">
				<div class="fb-share-button" data-href="<?php echo BASEURL.'home/share/'.$player_id;?>" data-layout="button" data-size="large" data-mobile-iframe="false"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Bagikan</a></div>
			</div>
		</div>
	</div>
	
	<input type="hidden" name="csrf_token" value="<?php echo $csrf;?>" />
	<script>var siteurl = '<?php echo SITEURL;?>';</script>
	<script src="<?php echo BASEURL;?>assets/js/jquery-3.2.0.min.js"></script>
	<script src="<?php echo BASEURL;?>assets/js/quiz.yudha.js"></script>
	<script>var continueQuiz = false;</script>
	<?php if ($download_file):?>
	<script>$(function(){ <?php if ($continue):?>continueQuiz = true;<?php endif;?> downloadFile();})</script>
	<?php endif;?>
	<?php if (! $download_file && $continue):?>
	<script>$(function(){ continueQuiz = true; $('#start').trigger('click'); })</script>
	<?php endif;?>
</body>
</html>
