<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?php echo $title;?> - onMarch.com - SCORE</title>
	<meta name="author" content="Yudha Rachmat Hidayat" />
	<meta name="designer" content="Yudha Rachmat Hidayat | email [at] yudha.web.id" />
	
	<!-- for facebook share -->
	<meta property="fb:app_id"		  content="300768647031761" />
	<meta property="og:url"           content="<?php echo BASEURL;?>home/share/<?php echo $player_id;?>" />
	<meta property="og:type"          content="website" />
	<meta property="og:title"         content="onMarch.com - SCORE" />
	<meta property="og:description"   content="Word Scrambler Quiz - DEMO." />
	<meta property="og:image"         content="<?php echo BASEURL;?>assets/img/icon.png" />
	
	<link rel="stylesheet" href="<?php echo BASEURL;?>assets/css/yudha.css" />
</head>
<body>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.9";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	
	<div class="as-table">
		<div class="as-td" align="center">
			<div class="player-id-container">
				Player ID : <span><?php echo $player_id;?></span>
			</div>
			<div class="score-container">
				Score : <span id="score"><?php echo $score;?></span><span id="add"></span>
			</div>
			<br />
			<a href="<?php echo SITEURL;?>">Home</a>
			<div style="margin-top: 20px;">
				<div class="fb-share-button" data-href="<?php echo BASEURL.'home/share/'.$player_id;?>" data-layout="button" data-size="large" data-mobile-iframe="false"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Bagikan</a></div>
			</div>
		</div>
	</div>
	
	<script>
		(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.9";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
	</script>
</body>
</html>
