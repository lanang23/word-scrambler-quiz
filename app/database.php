<?php if (! defined('BASEURL')) die('Forbidden');

/* Created by	: Yudha Rachmat Hidayat
 * Email		: email@yudha.web.id
 * Website		: https://yudha.id
 * 
 * */
 
$db_config = Array(
	'dbname' => 'data/wordscrambler.sqlite3'
);
